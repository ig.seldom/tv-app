package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"regexp"
	"strings"
	"time"
)

var proxyHost = os.Getenv("PROXY_HOST") + "/proxy?p="

func ResponseFromURL(url, referer string) ([]byte, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Referer", referer)

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	err = resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return body, nil
}

func GetStreamURL(name string) (string, error) {
	c, err := ResponseFromURL("http://cdntvpotok.com/onlytv/"+name+".php", "http://smotrite.only-tv.org")
	if err != nil {
		return "", err
	}

	s := FindProxy(string(c), "$1")

	return s, nil
}

func ReplaceStreamURLWithProxy(url string) string {
	return FindProxy(url, fmt.Sprintf("%s$1", proxyHost))
}

func HandleChannel(w http.ResponseWriter, r *http.Request, name string) {
	stream, err := GetStreamURL(name)
	if err != nil {
		log.Println(err)
	}
	stream = PrependProxy(stream)
	html := `
	<html>
	<head>
		<script src="//cdn.jsdelivr.net/npm/hls.js@latest"></script>
		<title>` + name + `</title>
	</head>
	<body style="margin: 0;padding: 0;background: #000;">
		<video id="video" style="width: 100%; height: 100%;" muted="muted"></video>
		<script>
			if (Hls.isSupported()) {
				const video = document.getElementById('video');
				const hls = new Hls()
				hls.loadSource('` + stream + `')
				hls.attachMedia(video);
				hls.on(Hls.Events.MEDIA_ATTACHED, () => {
					video.mute = true
					video.play()
				});
				
				video.addEventListener("click", (e) => {
					if(video.muted) {
						video.muted = false
						return
					}
					if(video.paused) {
						video.play()
					} else {
						video.pause()
					}
				})
			}
		</script>
	</body>
</html>
`
	fmt.Fprint(w, html)
}

func HandleRoot(w http.ResponseWriter, r *http.Request) {
	channel := r.URL.Query().Get("c")
	if len(channel) > 0 {
		HandleChannel(w, r, channel)
	} else {
		html := `
		<html>
	<head>
		<script src="//cdn.jsdelivr.net/npm/hls.js@latest"></script>
		<title>Channel list</title>
		<style>
			a, a:hover, a:visited {
				color: #fff;
				font-size: 24px;
			}
		</style>
	</head>
	<body style="margin: 0;padding: 0;background: #000;color:#fff">
<li><a href='/?c=tlc'>TLC смотреть онлайн</a></li>
<li><a href='/?c=fox'>FOX смотреть онлайн</a></li>
<li><a href='/?c=fox-life'>Fox Life смотреть онлайн</a></li>
<hr/>
		<li><a href='/?c=unikum'>Уникум смотреть онлайн</a></li>
<li><a href='/?c=cinema'>Cinema смотреть онлайн</a></li>
<li><a href='/?c=kinouzhas'>Киноужас смотреть онлайн</a></li>
<li><a href='/?c=dorama'>Дорама смотреть онлайн</a></li>
<li><a href='/?c=v-mire-zhivotnykh'>В мире животных смотреть онлайн</a></li>
<li><a href='/?c=filmbox'>FilmBox смотреть онлайн</a></li>
<li><a href='/?c=kapitan-fantastika'>Капитан Фантастика смотреть онлайн</a></li>
<li><a href='/?c=priklyucheniya-hd'>Приключения HD смотреть онлайн</a></li>
<li><a href='/?c=multilandiya'>Мультиландия смотреть онлайн</a></li>
<li><a href='/?c=bolt'>Bolt смотреть онлайн</a></li>
<li><a href='/?c=animal-planet'>Animal Planet смотреть онлайн</a></li>
<li><a href='/?c=match-boets'>Матч Боец смотреть онлайн</a></li>
<li><a href='/?c=discovery'>Discovery Channel смотреть онлайн</a></li>
<li><a href='/?c=match-futbol-1'>Матч Футбол 1 смотреть онлайн</a></li>
<li><a href='/?c=kinohit'>Кинохит смотреть онлайн</a></li>
<li><a href='/?c=eurosport'>Eurosport 1 смотреть онлайн</a></li>
<li><a href='/?c=cartoon-network'>Cartoon Network смотреть онлайн</a></li>
<li><a href='/?c=match-tv'>Матч ТВ смотреть онлайн</a></li>
<li><a href='/?c=tv-3'>ТВ3 смотреть онлайн</a></li>
<li><a href='/?c=dozhd'>Дождь смотреть онлайн</a></li>
<li><a href='/?c=match-premer'>Матч Премьер смотреть онлайн</a></li>
<li><a href='/?c=khl-tv'>КХЛ ТВ смотреть онлайн</a></li>
<li><a href='/?c=kinopremera'>Кинопремьера смотреть онлайн</a></li>
<li><a href='/?c=eurosport-2'>Евроспорт 2 смотреть онлайн</a></li>
<li><a href='/?c=vip-megahit'>ViP Megahit смотреть онлайн</a></li>
<li><a href='/?c=vip-comedy'>ViP Comedy смотреть онлайн</a></li>
<li><a href='/?c=vip-premiere'>ViP Premiere смотреть онлайн</a></li>
<li><a href='/?c=tv1000-action'>TV1000 Action смотреть онлайн</a></li>
<li><a href='/?c=tv1000'>ТВ1000 смотреть онлайн</a></li>
<li><a href='/?c=match-futbol-2'>Матч Футбол 2 смотреть онлайн</a></li>
<li><a href='/?c=kinosemya'>Киносемья смотреть онлайн</a></li>
<li><a href='/?c=match-arena'>Матч Арена смотреть онлайн</a></li>
<li><a href='/?c=id-xtra'>ID Xtra смотреть онлайн</a></li>
<li><a href='/?c=boomerang'>Бумеранг смотреть онлайн</a></li>
<li><a href='/?c=nashe-krutoe'>Наше Крутое смотреть онлайн</a></li>
<li><a href='/?c=premialnoe-hd'>Премиальное HD смотреть онлайн</a></li>
<li><a href='/?c=morskoj'>Морской канал смотреть онлайн</a></li>
<li><a href='/?c=tajny-galaktiki'>Тайны Галактики смотреть онлайн</a></li>
<li><a href='/?c=tnt4'>ТНТ4 смотреть онлайн</a></li>
<li><a href='/?c=tnt'>ТНТ смотреть онлайн</a></li>
<li><a href='/?c=sts'>СТС смотреть онлайн</a></li>
<li><a href='/?c=novyj-kanal'>Новый канал смотреть онлайн</a></li>
<li><a href='/?c=tv1000-russkoe-kino'>ТВ1000 Русское кино смотреть онлайн</a></li>
<li><a href='/?c=nickelodeon'>Nickelodeon смотреть онлайн</a></li>
<li><a href='/?c=stb'>СТБ смотреть онлайн</a></li>
<li><a href='/?c=rossiya-1'>Россия 1 смотреть онлайн</a></li>
<li><a href='/?c=kinomiks'>Киномикс смотреть онлайн</a></li>
<li><a href='/?c=amedia-hit'>Amedia Hit смотреть онлайн</a></li>
<li><a href='/?c=tnt-music'>ТНТ Music смотреть онлайн</a></li>
<li><a href='/?c=zhara-tv'>Жара ТВ смотреть онлайн</a></li>
<li><a href='/?c=romantichnoe-hd'>Романтичное HD смотреть онлайн</a></li>
<li><a href='/?c=kinosat'>КиноСат смотреть онлайн</a></li>
<li><a href='/?c=mezzo-live-hd'>Mezzo Live HD смотреть онлайн</a></li>
<li><a href='/?c=kvn-tv'>КВН ТВ смотреть онлайн</a></li>
<li><a href='/?c=kinoseriya'>Киносерия смотреть онлайн</a></li>
<li><a href='/?c=zhivaya-planeta'>Живая планета смотреть онлайн</a></li>
<li><a href='/?c=tochka-otryva'>Точка отрыва смотреть онлайн</a></li>
<li><a href='/?c=mega'>Мега смотреть онлайн</a></li>
<li><a href='/?c=tet'>ТЕТ смотреть онлайн</a></li>
<li><a href='/?c=viasat-history'>Viasat History смотреть онлайн</a></li>
<li><a href='/?c=viasat-explorer'>Viasat Explorer смотреть онлайн</a></li>
<li><a href='/?c=viasat-nature'>Viasat Nature смотреть онлайн</a></li>
<li><a href='/?c=setanta-sports-ukraine'>Setanta Sports Ukraine смотреть онлайн</a></li>
<li><a href='/?c=futbol-1-ukraina'>Футбол 1 (Украина) смотреть онлайн</a></li>
<li><a href='/?c=ren'>РЕН ТВ смотреть онлайн</a></li>
<li><a href='/?c=pervyj-kanal'>Первый канал смотреть онлайн</a></li>
<li><a href='/?c=pyatnitsa-tv'>Пятница смотреть онлайн</a></li>
<li><a href='/?c=superkanal'>Супер канал смотреть онлайн</a></li>
<li><a href='/?c=pyatyj-kanal'>Пятый канал смотреть онлайн</a></li>
<li><a href='/?c=kinosvidanie'>Киносвидание смотреть онлайн</a></li>
<li><a href='/?c=match-futbol-3'>Матч Футбол 3 смотреть онлайн</a></li>
<li><a href='/?c=match-igra'>Матч Игра смотреть онлайн</a></li>
<li><a href='/?c=match-planeta'>Матч Планета смотреть онлайн</a></li>
<li><a href='/?c=match-nash-sport'>Матч Наш Спорт смотреть онлайн</a></li>
<li><a href='/?c=discovery-science'>Discovery science смотреть онлайн</a></li>
<li><a href='/?c=dtx'>DTX смотреть онлайн</a></li>
<li><a href='/?c=trk-kiev'>Киев ТВ смотреть онлайн</a></li>
<li><a href='/?c=rodnoe-kino'>Родное кино смотреть онлайн</a></li>
<li><a href='/?c=nashe-novoe-kino'>Наше новое кино смотреть онлайн</a></li>
<li><a href='/?c=fightbox-hd'>FightBox HD смотреть онлайн</a></li>
<li><a href='/?c=nat-geo-wild'>Nat Geo Wild смотреть онлайн</a></li>
<li><a href='/?c=ru-tv'>RU TV смотреть онлайн</a></li>
<li><a href='/?c=kino-tv'>Кино ТВ смотреть онлайн</a></li>
<li><a href='/?c=nst'>НСТ смотреть онлайн</a></li>
<li><a href='/?c=tv-xxi'>TV 21 смотреть онлайн</a></li>
<li><a href='/?c=oruzhie'>Оружие смотреть онлайн</a></li>
<li><a href='/?c=1-plus-1'>1+1 смотреть онлайн</a></li>
<li><a href='/?c=ictv'>ICTV смотреть онлайн</a></li>
<li><a href='/?c=ntv'>НТВ смотреть онлайн</a></li>
<li><a href='/?c=domashnij'>Домашний смотреть онлайн</a></li>
<li><a href='/?c=24-tekhno'>24 Техно смотреть онлайн</a></li>
<li><a href='/?c=trk-ukraina'>ТРК Украина смотреть онлайн</a></li>
<li><a href='/?c=disney'>Disney смотреть онлайн</a></li>
<li><a href='/?c=inter'>Интер смотреть онлайн</a></li>
<li><a href='/?c=muz-tv'>Муз-ТВ смотреть онлайн</a></li>
<li><a href='/?c=otr'>ОТР смотреть онлайн</a></li>
<li><a href='/?c=russkaya-komediya'>Русская комедия смотреть онлайн</a></li>
<li><a href='/?c=mult'>Мульт смотреть онлайн</a></li>
<li><a href='/?c=spike'>Spike смотреть онлайн</a></li>
<li><a href='/?c=sts-love'>СТС Love смотреть онлайн</a></li>
<li><a href='/?c=otse'>ОЦЕ смотреть онлайн</a></li>
<li><a href='/?c=2x2'>2х2 смотреть онлайн</a></li>
<li><a href='/?c=kanal-che'>Канал Че смотреть онлайн</a></li>
<li><a href='/?c=bt-sport-espn'>BT Sport ESPN смотреть онлайн</a></li>
<li><a href='/?c=kinokomediya'>Кинокомедия смотреть онлайн</a></li>
<li><a href='/?c=2-plus-2'>2+2 смотреть онлайн</a></li>
<li><a href='/?c=football-2'>Футбол 2 смотреть онлайн</a></li>
<li><a href='/?c=illyuzion-plus'>Иллюзион Плюс смотреть онлайн</a></li>
<li><a href='/?c=ntn'>НТН смотреть онлайн</a></li>
<li><a href='/?c=istoriya'>История смотреть онлайн</a></li>
<li><a href='/?c=history-channel'>History смотреть онлайн</a></li>
<li><a href='/?c=rossiya-24'>Россия 24 смотреть онлайн</a></li>
<li><a href='/?c=zee-tv'>Zee TV смотреть онлайн</a></li>
<li><a href='/?c=bober-tv'>Бобер ТВ смотреть онлайн</a></li>
<li><a href='/?c=national-geographic'>National Geographic смотреть онлайн</a></li>
<li><a href='/?c=kvartal-tv'>Квартал ТВ смотреть онлайн</a></li>
<li><a href='/?c=moya-planeta'>Моя планета смотреть онлайн</a></li>
<li><a href='/?c=karusel'>Карусель смотреть онлайн</a></li>
<li><a href='/?c=zvezda'>Звезда смотреть онлайн</a></li>
<li><a href='/?c=sovershenno-sekretno'>Совершенно секретно смотреть онлайн</a></li>
<li><a href='/?c=k1'>К1 смотреть онлайн</a></li>
<li><a href='/?c=k2'>К2 смотреть онлайн</a></li>
<li><a href='/?c=112-kanal'>112 канал смотреть онлайн</a></li>
<li><a href='/?c=russkij-illyuzion'>Русский Иллюзион смотреть онлайн</a></li>
<li><a href='/?c=nauka-2'>Наука 2.0 смотреть онлайн</a></li>
<li><a href='/?c=a1'>Канал A1 смотреть онлайн</a></li>
<li><a href='/?c=a2'>Канал A2 смотреть онлайн</a></li>
<li><a href='/?c=amedia-premium-hd'>Amedia Premium HD смотреть онлайн</a></li>
<li><a href='/?c=tvc'>ТВЦ смотреть онлайн</a></li>
<li><a href='/?c=rtr-planeta'>РТР Планета смотреть онлайн</a></li>
<li><a href='/?c=paramount-comedy'>Paramount Comedy смотреть онлайн</a></li>
<li><a href='/?c=rbk'>РБК смотреть онлайн</a></li>
<li><a href='/?c=tlum-hd'>Тлум HD смотреть онлайн</a></li>
<li><a href='/?c=moskva-24'>Москва 24 смотреть онлайн</a></li>
<li><a href='/?c=russkij-bestseller'>Русский бестселлер смотреть онлайн</a></li>
<li><a href='/?c=russkij-roman'>Русский роман смотреть онлайн</a></li>
<li><a href='/?c=russkij-detektiv'>Русский детектив смотреть онлайн</a></li>
<li><a href='/?c=mtv-russia'>MTV Россия смотреть онлайн</a></li>
<li><a href='/?c=russian-music-box'>Russian Music Box смотреть онлайн</a></li>
<li><a href='/?c=europa-plus-tv'>Европа Плюс ТВ смотреть онлайн</a></li>
<li><a href='/?c=bigudi'>Бигуди смотреть онлайн</a></li>
<li><a href='/?c=detskij'>Детский канал смотреть онлайн</a></li>
<li><a href='/?c=avto-24'>Авто 24 смотреть онлайн</a></li>
<li><a href='/?c=auto-plus'>Авто плюс смотреть онлайн</a></li>
<li><a href='/?c=muzyka-pervogo'>Музыка Первого смотреть онлайн</a></li>
<li><a href='/?c=shanson-tv'>Шансон ТВ смотреть онлайн</a></li>
<li><a href='/?c=mir'>Мир смотреть онлайн</a></li>
<li><a href='/?c=mezzo'>Mezzo смотреть онлайн</a></li>
<li><a href='/?c=dom-kino'>Дом Кино смотреть онлайн</a></li>
<li><a href='/?c=xsport'>XSport смотреть онлайн</a></li>
<li><a href='/?c=malyatko-tv'>Малятко ТВ смотреть онлайн</a></li>
<li><a href='/?c=bridge-tv'>Бридж ТВ смотреть онлайн</a></li>
<li><a href='/?c=motorsport-tv'>Моторспорт ТВ смотреть онлайн</a></li>
<li><a href='/?c=cnn'>CNN смотреть онлайн</a></li>
<li><a href='/?c=plus-plus'>ПлюсПлюс смотреть онлайн</a></li>
<li><a href='/?c=russia-today'>Russia Today смотреть онлайн</a></li>
<li><a href='/?c=super-tennis-hd'>Super Tennis HD смотреть онлайн</a></li>
<li><a href='/?c=muzhskoj'>Мужской смотреть онлайн</a></li>
<li><a href='/?c=8-kanal'>8 канал смотреть онлайн</a></li>
<li><a href='/?c=strashnoe-hd'>Страшное HD смотреть онлайн</a></li>
<li><a href='/?c=kanal-you'>Канал Ю смотреть онлайн</a></li>
<li><a href='/?c=kazsport'>КАЗспорт смотреть онлайн</a></li>
<li><a href='/?c=belarus-5'>Беларусь 5 смотреть онлайн</a></li>
<li><a href='/?c=okhota-i-rybalka'>Охота и рыбалка смотреть онлайн</a></li>
<li><a href='/?c=zik'>ZIK смотреть онлайн</a></li>
<li><a href='/?c=drajv-tv'>Драйв ТВ смотреть онлайн</a></li>
<li><a href='/?c=rossiya-kultura'>Россия К (Культура) смотреть онлайн</a></li>
<li><a href='/?c=okhotnik-i-rybolov'>Охотник и рыболов смотреть онлайн</a></li>
<li><a href='/?c=euronews'>EuroNews смотреть онлайн</a></li>
<li><a href='/?c=newsone'>News One смотреть онлайн</a></li>
<li><a href='/?c=24-ukraina'>24 Украина смотреть онлайн</a></li>
<li><a href='/?c=espreso-tv'>Еспресо ТВ смотреть онлайн</a></li>
<li><a href='/?c=5-kanal-ukraina'>5 канал Украина смотреть онлайн</a></li>
<li><a href='/?c=gromadske-tv'>Громадське ТВ смотреть онлайн</a></li>
<li><a href='/?c=ostrosyuzhetnoe-hd'>Остросюжетное HD смотреть онлайн</a></li>
<li><a href='/?c=nash-kinoroman'>Наш Кинороман смотреть онлайн</a></li>
<li><a href='/?c=evrokino'>Еврокино смотреть онлайн</a></li>
<li><a href='/?c=sport-1-ukraina'>Спорт 1 Украина смотреть онлайн</a></li>
<li><a href='/?c=sport-2-ukraina'>Спорт 2 Украина смотреть онлайн</a></li>
<li><a href='/?c=boks-tv'>Бокс ТВ смотреть онлайн</a></li>
<li><a href='/?c=24-dok'>Телеканал Доктор смотреть онлайн</a></li>
<li><a href='/?c=indijskoe-kino'>Индийское кино смотреть онлайн</a></li>
<li><a href='/?c=nano-tv'>Нано ТВ смотреть онлайн</a></li>
<li><a href='/?c=playboy-tv'>Playboy TV смотреть онлайн</a></li>
<li><a href='/?c=tiji'>Tiji смотреть онлайн</a></li>
<li><a href='/?c=gulli'>Gulli Girl смотреть онлайн</a></li>
<li><a href='/?c=radost-moya'>Радость моя смотреть онлайн</a></li>
<li><a href='/?c=fine-living'>Fine Living смотреть онлайн</a></li>
<li><a href='/?c=telekanal-futbol'>Телеканал Футбол смотреть онлайн</a></li>
<li><a href='/?c=hollywood-hd'>Hollywood HD смотреть онлайн</a></li>
<li><a href='/?c=russkij-ekstrim'>Русский Экстрим смотреть онлайн</a></li>
<li><a href='/?c=eda-tv'>Еда ТВ смотреть онлайн</a></li>
<li><a href='/?c=setanta-sports-1'>Setanta Sports 1 смотреть онлайн</a></li>
<li><a href='/?c=piksel'>Пиксель смотреть онлайн</a></li>
<li><a href='/?c=travel-channel'>Travel Channel смотреть онлайн</a></li>
<li><a href='/?c=m1'>М1 смотреть онлайн</a></li>
<li><a href='/?c=m2'>М2 смотреть онлайн</a></li>
<li><a href='/?c=hardlife-tv'>HardLife TV смотреть онлайн</a></li>
<li><a href='/?c=outdoor-channel'>Outdoor Channel смотреть онлайн</a></li>
<li><a href='/?c=nick-jr'>Nick Jr смотреть онлайн</a></li>
<li><a href='/?c=vh1'>VH1 смотреть онлайн</a></li>
<li><a href='/?c=travel-and-adventure'>Travel and Adventure смотреть онлайн</a></li>
<li><a href='/?c=krym-1'>Первый Крымский смотреть онлайн</a></li>
<li><a href='/?c=setanta-sports-2'>Setanta Sports 2 смотреть онлайн</a></li>
<li><a href='/?c=ntv-serial'>НТВ Сериал смотреть онлайн</a></li>
<li><a href='/?c=bbc-news'>BBC News смотреть онлайн</a></li>
<li><a href='/?c=mama'>Канал Мама смотреть онлайн</a></li>
<li><a href='/?c=sony-sci-fi'>Sony Sci-Fi смотреть онлайн</a></li>
<li><a href='/?c=sony-turbo'>Sony Turbo смотреть онлайн</a></li>
<li><a href='/?c=set'>SET смотреть онлайн</a></li>
<li><a href='/?c=ntv-hit'>НТВ‑ХИТ смотреть онлайн</a></li>
<li><a href='/?c=kanal-strana'>Мультимузыка смотреть онлайн</a></li>
<li><a href='/?c=360-gradusov'>360 градусов смотреть онлайн</a></li>
<li><a href='/?c=sarafan-tv'>Сарафан ТВ смотреть онлайн</a></li>
<li><a href='/?c=tdk'>ТДК смотреть онлайн</a></li>
<li><a href='/?c=konnyj-mir'>Конный мир смотреть онлайн</a></li>
<li><a href='/?c=ntv-mir'>НТВ Мир смотреть онлайн</a></li>
<li><a href='/?c=kinopokaz'>Кинопоказ смотреть онлайн</a></li>
<li><a href='/?c=9-kanal-izrail'>9 канал Израиль смотреть онлайн</a></li>
<li><a href='/?c=krasnaya-liniya'>Красная линия смотреть онлайн</a></li>
<li><a href='/?c=bollywood'>Bollywood смотреть онлайн</a></li>
<li><a href='/?c=tochka-tv'>Точка ТВ смотреть онлайн</a></li>
<li><a href='/?c=extreme-sports'>Extreme Sports смотреть онлайн</a></li>
<li><a href='/?c=kanal-o'>Канал О! смотреть онлайн</a></li>
<li><a href='/?c=mir-seriala'>Мир сериала смотреть онлайн</a></li>
<li><a href='/?c=paramount-channel'>Paramount Channel смотреть онлайн</a></li>
<li><a href='/?c=vesti-fm'>Вести ФМ смотреть онлайн</a></li>
<li><a href='/?c=shokiruyushchee-hd'>Шокирующее HD смотреть онлайн</a></li>
<li><a href='/?c=komedijnoe-hd'>Комедийное HD смотреть онлайн</a></li>
<li><a href='/?c=world-fashion'>World Fashion смотреть онлайн</a></li>
<li><a href='/?c=dom-kino-premium'>Дом кино Премиум смотреть онлайн</a></li>
<li><a href='/?c=sila-tv'>СИЛА ТВ смотреть онлайн</a></li>
<li><a href='/?c=lyubimoe-kino'>Любимое Кино смотреть онлайн</a></li>
<li><a href='/?c=hd-life'>HD Life смотреть онлайн</a></li>
<li><a href='/?c=trofej'>Трофей смотреть онлайн</a></li>
<li><a href='/?c=ekho-tv'>Эхо ТВ смотреть онлайн</a></li>
<li><a href='/?c=mir-24'>Мир 24 смотреть онлайн</a></li>
<li><a href='/?c=ufc-tv'>UFC ТВ смотреть онлайн</a></li>
<li><a href='/?c=ani'>Ani смотреть онлайн</a></li>
<li><a href='/?c=jimjam'>JimJam смотреть онлайн</a></li>
<li><a href='/?c=lale'>Lale смотреть онлайн</a></li>
<li><a href='/?c=detskij-mir'>Детский мир смотреть онлайн</a></li>
<li><a href='/?c=bloomberg'>Bloomberg смотреть онлайн</a></li>
<li><a href='/?c=zoom'>Zoom смотреть онлайн</a></li>
<li><a href='/?c=zagorodnaya-zhizn'>Загородная жизнь смотреть онлайн</a></li>
<li><a href='/?c=zoo-tv'>Зоо ТВ смотреть онлайн</a></li>
<li><a href='/?c=rtg'>RTG смотреть онлайн</a></li>
<li><a href='/?c=ua-pervyj'>UA Первый смотреть онлайн</a></li>
<li><a href='/?c=telekanal-pryamoj'>Телеканал Прямой смотреть онлайн</a></li>
<li><a href='/?c=dange-tv'>Dange TV смотреть онлайн</a></li>
<li><a href='/?c=mir-basketbola'>Мир Баскетбола смотреть онлайн</a></li>
<li><a href='/?c=m-1-global'>M‑1 Global смотреть онлайн</a></li>
<li><a href='/?c=sky-sports-premier-league'>Sky Sports Premier League смотреть онлайн</a></li>
<li><a href='/?c=sky-sports-main-event'>Sky Sports Main Event смотреть онлайн</a></li>
<li><a href='/?c=bt-sport-1'>BT Sport 1 смотреть онлайн</a></li>
<li><a href='/?c=bt-sport-2'>BT Sport 2 смотреть онлайн</a></li>
<li><a href='/?c=bt-sport-3'>BT Sport 3 смотреть онлайн</a></li>
<li><a href='/?c=arena-sport-1'>Arena Sport 1 смотреть онлайн</a></li>
<li><a href='/?c=arena-sport-2'>Arena Sport 2 смотреть онлайн</a></li>
<li><a href='/?c=arena-sport-3'>Arena Sport 3 смотреть онлайн</a></li>
<li><a href='/?c=arena-sport-4'>Arena Sport 4 смотреть онлайн</a></li>
<li><a href='/?c=arena-sport-5'>Arena Sport 5 смотреть онлайн</a></li>
<li><a href='/?c=bein-sports-1'>BeIN Sports 1 смотреть онлайн</a></li>
<li><a href='/?c=bein-sports-2'>BeIN Sports 2 смотреть онлайн</a></li>
<li><a href='/?c=bein-sports-3'>BeIN Sports 3 смотреть онлайн</a></li>
<li><a href='/?c=look-sport-tv'>Look Sport TV смотреть онлайн</a></li>
<li><a href='/?c=canal-sport'>Canal+ Sport смотреть онлайн</a></li>
<li><a href='/?c=sport-tv-1'>Sport TV 1 смотреть онлайн</a></li>
<li><a href='/?c=sport-tv-2'>Sport TV 2 смотреть онлайн</a></li>
<li><a href='/?c=sport-tv-3'>Sport TV 3 смотреть онлайн</a></li>
<li><a href='/?c=sport-tv-4'>Sport TV 4 смотреть онлайн</a></li>
<li><a href='/?c=sport-tv-5'>Sport TV 5 смотреть онлайн</a></li>
<li><a href='/?c=sport-klub-1'>Sport Klub 1 смотреть онлайн</a></li>
<li><a href='/?c=sport-klub-2'>Sport Klub 2 смотреть онлайн</a></li>
<li><a href='/?c=sport-klub-3'>Sport Klub 3 смотреть онлайн</a></li>
<li><a href='/?c=supersport-2'>Supersport 2 смотреть онлайн</a></li>
<li><a href='/?c=supersport-3'>Supersport 3 смотреть онлайн</a></li>
</ul>
</body>
</html>
		`
		fmt.Fprint(w, html)
	}
}

func HandleTLC(w http.ResponseWriter, r *http.Request) {
	HandleChannel(w, r, "tlc")
}

func HandleFoxLife(w http.ResponseWriter, r *http.Request) {
	HandleChannel(w, r, "fox-life")
}

func ReplaceToProxy(from, to string) string {
	m1 := regexp.MustCompile(`file:"(.*)"`)
	return m1.ReplaceAllString(from, to)
}

func FindProxy(from, to string) string {
	m1 := regexp.MustCompile(`file:"(.*)"`)
	s := m1.FindString(from)
	return ReplaceToProxy(s, to)
}

func PrependProxy(u string) string {
	return fmt.Sprintf("%s%s", proxyHost, url.QueryEscape(u))
}

func PrependURLToPlaylist(from, to string) string {
	m1 := regexp.MustCompile(`^([a-zA-Z0-9].*)`)
	ss := strings.Split(from, "\n")
	rep := []string{}
	for _, s := range ss {
		rep = append(rep, m1.ReplaceAllString(s, to))
	}
	return strings.Join(rep, "\n")
}

func HandleProxy(w http.ResponseWriter, r *http.Request) {
	startTime := time.Now()
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	proxy := r.URL.Query().Get("p")
	req, err := http.NewRequest(http.MethodGet, proxy, nil)
	if err != nil {
		r.Response.Status = http.StatusText(http.StatusInternalServerError)
		fmt.Fprintf(w, "%v", err)
		return
	}
	req.Header.Add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36")
	if strings.Contains(proxy, ".m3u8") {
		req.Header.Add("Referer", "http://smotrite.only-tv.org")
	} else {
		req.Header.Add("Origin", "http://cdntvpotok.com")
		req.Header.Add("Referer", "http://cdntvpotok.com")
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		r.Response.Status = http.StatusText(http.StatusInternalServerError)
		fmt.Fprintf(w, "%v", err)
		return
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Println(err)
		}
	}()

	c, err := io.ReadAll(resp.Body)
	if err != nil {
		r.Response.Status = http.StatusText(http.StatusInternalServerError)
		fmt.Fprintf(w, "%v", err)
		return
	}
	s := string(c)
	if strings.Contains(proxy, ".m3u8") {
		u, err := url.Parse(proxy)
		if err != nil {
			r.Response.Status = http.StatusText(http.StatusInternalServerError)
			fmt.Fprintf(w, "%v", err)
			return
		}

		s = PrependURLToPlaylist(s, fmt.Sprintf("%s%s://%s%s/$1", proxyHost, u.Scheme, u.Host, path.Dir(resp.Request.URL.Path)))
	}

	fmt.Fprint(w, s)
	endTime := time.Now()

	diffTime := endTime.Sub(startTime)

	log.Printf("Loading url %s took %v", proxy, diffTime)
}

func main() {
	http.HandleFunc("/", HandleRoot)
	http.HandleFunc("/fox-life", HandleFoxLife)
	http.HandleFunc("/tlc", HandleTLC)
	http.HandleFunc("/proxy", HandleProxy)
	fmt.Println("Using proxy host", proxyHost)
	http.ListenAndServe(":8020", nil)
}
