FROM golang:1.20 as builder
WORKDIR /app
COPY ./src .
RUN go mod vendor && \
    go build -a -o tvapp . && \
    chmod +x tvapp
CMD [ "/app/tvapp" ]